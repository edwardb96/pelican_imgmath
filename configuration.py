# -*- coding: utf-8 -*-
"""
    pelican_imgmath
    ~~~~~~~~~~~~~~~~~~
    Render math in HTML via dvipng or dvisvgm.

    :copyright: Copyright 2007-2018 by the Sphinx team and Edward Brown, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

from os import path

default_settings = {
    'image_format': 'svg',
    'add_tooltips': True,
    'font_size': 12,
    'dvipng': {
        'command' : 'dvipng',
        'args' : ['-gamma',
                  '1.5',
                  '-D',
                  '110',
                  '-bg',
                  'Transparent']
    },
    'dvisvgm': {
        'command': 'dvisvgm',
        'args': ['--no-fonts']
    },
    'latex': {
        'command': 'latex',
        'args': [],
        'preamble': '',
        'use_preview': False
    }
}

def user_config_variable(pelicanobj, var, typename):
    try:
        settings = pelicanobj.settings[var]
        if isinstance(settings, typename):
            return settings
        else:
            return None
    except:
        return None

def compulsory_user_config_variable(pelicanobj, var, typename):
    value = user_config_variable(pelicanobj, var, typename)
    if value is not None:
        return value
    else:
        raise RuntimeError("Missing configuration variable: '{}'".format(var))

def merge_settings(a, b):
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_settings(a[key], b[key])
            elif a[key] == b[key]:
                pass # same leaf value
            else:
                a[key] = b[key]
        else:
            a[key] = b[key]
    return a

def default_output_directory():
    return path.join('images', 'math')

def canonical_settings_as_dictionary(pelicanobj):
    user_settings = user_config_variable(pelicanobj, 'IMGMATH', dict)
    if user_settings is None:
        return default_settings
    else:
        merge_settings(default_settings, user_settings)
        if 'output_directory' not in default_settings.keys():
            default_settings['output_directory'] = default_output_directory()
        return default_settings

def compulsory(key, typename, settings):
    parts = key.split('.')
    for part in parts[:-1]:
        if part in settings.keys():
            if isinstance(settings[part], dict):
                settings = settings[part]
            else:
                raise RuntimeError('Expected setting {} to be a dict.'.format(key))
        else:
            raise RuntimeError('Missing compulsory setting {}.'.format(key))

    lastpart = parts[-1]
    if lastpart in settings.keys():
        value = settings[lastpart]
        if isinstance(value, typename):
            return value
        else:
            raise RuntimeError('Expected setting {} to hold a value of type {}.'.format(key, typename.__name__))
    else:
        raise RuntimeError('Missing compulsory setting {}.'.format(key))
