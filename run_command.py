# -*- coding: utf-8 -*-
"""
    pelican_imgmath
    ~~~~~~~~~~~~~~~~~~
    Render math in HTML via dvipng or dvisvgm.

    :copyright: Copyright 2007-2018 by the Sphinx team and Edward Brown, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

from subprocess import Popen, PIPE
import contextlib
import os
import errno

ENOENT = getattr(errno, 'ENOENT', 0)


def run_command(command_name, command):
    try:
        p = Popen(command, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            return False, '''
{} exited with error.
[stderr]:
{}
[stdout]:
{}
'''.format(command_name, stderr, stdout)

        else:
            return True, None
    except OSError as err:
        if err.errno == ENOENT:   # No such file or directory
            return False, "{} command cannot be run (needed for math display) check the IMGMATH.{}.command setting".format(command_name, command_name)
        else:
            raise

def run_command_in_directory(command_name, command, directory):
    with cd(directory):
        return run_command(command_name, command)

@contextlib.contextmanager
def cd(target_dir):
    cwd = os.getcwd()
    try:
        os.chdir(target_dir)
        yield
    finally:
        os.chdir(cwd)
