# -*- coding: utf-8 -*-
"""
    pelican_imgmath
    ~~~~~~~~~~~~~~~~~~
    Render math in HTML via dvipng or dvisvgm.

    :copyright: Copyright 2007-2018 by the Sphinx team and Edward Brown, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

from os import path
import re
import binascii
import struct

from .run_command import run_command
from six import PY3
import sys

# from sphinx.util.pycompat import sys_encoding
# sys_encoding: some kind of default system encoding; should be used with
# a lenient error handler
if PY3:
    sys_encoding = sys.getdefaultencoding()
else:
    sys_encoding = __import__('locale').getpreferredencoding()

def make_dvi_to_svg_converter(tempdir,
                              dvisvgm_command,
                              dvisvgm_command_args):
    def dvi_to_svg(input_filepath):
        output_filepath = path.join(tempdir, 'math.svg')
        command = build_dvisvgm_command(dvisvgm_command,
                                        input_filepath,
                                        output_filepath,
                                        dvisvgm_command_args)
        is_success, error_message = run_command('dvisvgm', command)
        if is_success:
            return output_filepath, None
        else:
            raise RuntimeError(error_message)

    return dvi_to_svg

def build_dvisvgm_command(command_name,
                          input_filepath,
                          output_filepath,
                          command_args):
    command = [command_name, '-o', output_filepath]
    command.extend(command_args)
    command.append(input_filepath)
    return command

def make_dvi_to_png_converter(tempdir,
                              dvipng_command,
                              dvipng_command_args,
                              use_preview):
    def dvi_to_png(input_filepath):
        output_filepath = path.join(tempdir, 'math.png')
        command = build_dvipng_command(dvipng_command,
                                       input_filepath,
                                       output_filepath,
                                       dvipng_command_args,
                                       use_preview)
        is_success, error_message = run_command('dvipng', command)
        if is_success:
            if use_preview:
                depth = write_depth_from_stdout_to_file(stdout, output_filepath)
                return output_filepath, depth
            else:
                return output_filepath, None
        else:
            raise RuntimeError(error_message)

    return dvi_to_png


def write_depth_from_stdout_to_file(stdout, filepath):
    depth = None
    for line in stdout.splitlines():
        matched = depth_re.match(line)
        if matched:
            depth = int(matched.group(1))
            write_png_depth(filepath, depth)
            return depth
    return depth

def read_png_depth(filename):
    """Read the special tEXt chunk indicating the depth from a PNG file."""
    with open(filename, 'rb') as f:
        f.seek(- (LEN_IEND + LEN_DEPTH), 2)
        depthchunk = f.read(LEN_DEPTH)
        if not depthchunk.startswith(DEPTH_CHUNK_LEN + DEPTH_CHUNK_START):
            # either not a PNG file or not containing the depth chunk
            return None
        else:
            return struct.unpack('!i', depthchunk[14:18])[0]

LEN_IEND = 12
LEN_DEPTH = 22

DEPTH_CHUNK_LEN = struct.pack('!i', 10)
DEPTH_CHUNK_START = b'tEXtDepth\x00'
IEND_CHUNK = b'\x00\x00\x00\x00IEND\xAE\x42\x60\x82'
depth_re = re.compile(br'\[\d+ depth=(-?\d+)\]')

def write_png_depth(filename, depth):
    """Write the special tEXt chunk indicating the depth to a PNG file.

    The chunk is placed immediately before the special IEND chunk.
    """
    data = struct.pack('!i', depth)
    with open(filename, 'r+b') as f:
        # seek to the beginning of the IEND chunk
        f.seek(-LEN_IEND, 2)
        # overwrite it with the depth chunk
        f.write(DEPTH_CHUNK_LEN + DEPTH_CHUNK_START + data)
        # calculate the checksum over chunk name and data
        crc = binascii.crc32(DEPTH_CHUNK_START + data) & 0xffffffff
        f.write(struct.pack('!I', crc))
        # replace the IEND chunk
        f.write(IEND_CHUNK)


def build_dvipng_command(command_name,
                         input_filepath,
                         output_filepath,
                         command_args,
                         use_preview):
    command = [command_name, '-o', output_filepath, '-T', 'tight', '-z9']
    command.extend(command_args)
    if use_preview:
        command.append('--depth')
    command.append(input_filepath)
    return command

def convert_dvi_to_image(command, name):
    return run_command(name, command)
