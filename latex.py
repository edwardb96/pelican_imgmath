# -*- coding: utf-8 -*-
"""
    pelican_imgmath
    ~~~~~~~~~~~~~~~~~~
    Render math in HTML via dvipng or dvisvgm.

    :copyright: Copyright 2007-2018 by the Sphinx team and Edward Brown, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

from os import path
from .run_command import run_command_in_directory

import codecs


def make_latex_to_dvi_converter(temporary_directory, latex_command_name, latex_command_args):
    def latex_to_dvi(latex):
        filepath = path.join(temporary_directory, 'math.tex')
        save_to_file(latex, filepath)

        command = build_latex_command(latex_command_name, filepath, latex_command_args)
        is_sucess, error_message = run_command_in_directory(latex_command_name, command, temporary_directory)

        if is_sucess:
            return path.join(temporary_directory, 'math.dvi')
        else:
            raise RuntimeError(error_message)

    return latex_to_dvi

def build_latex_command(command_name, latex_filepath, command_args):
    # build latex command; old versions of latex don't have the
    # --output-directory option, so we have to manually chdir to the
    # temp dir to run it.
    command = [command_name, '--interaction=nonstopmode']
    # add custom args from the config file
    command.extend(command_args)
    command.append(latex_filepath)
    return command


def save_to_file(utf8text, output_filepath):
    with codecs.open(output_filepath, 'w', 'utf-8') as f:
        f.write(utf8text)

DOC_HEAD = r'''
\documentclass[12pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{anyfontsize}
\usepackage{bm}
\pagestyle{empty}
'''

DOC_BODY = r'''
\begin{document}
\fontsize{%d}{%d}\selectfont %s
\end{document}
'''

DOC_BODY_PREVIEW = r'''
\usepackage[active]{preview}
\begin{document}
\begin{preview}
\fontsize{%s}{%s}\selectfont %s
\end{preview}
\end{document}
'''

def make_latex_macro_generator(font_size, preamble, use_preview):
    def generate_latex_macro(math):
        baselineskip = int(round(font_size * 1.2))
        return DOC_HEAD + preamble + DOC_BODY_PREVIEW % (font_size, baselineskip, "${}$".format(math)) if use_preview \
            else DOC_HEAD + preamble + DOC_BODY % (font_size, baselineskip, "${}$".format(math))

    return generate_latex_macro
