# -*- coding: utf-8 -*-
"""
    pelican_imgmath
    ~~~~~~~~~~~~~~~~~~
    Render math in HTML via dvipng or dvisvgm.

    :copyright: Copyright 2007-2018 by the Sphinx team and Edward Brown, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

import functools
from os import path
from bs4 import BeautifulSoup

def make_article_processor(generate_math_image, pelican_output_path):
    def process_article(article):
        content = article._content

        content_parsed = BeautifulSoup(article._content, 'html.parser')

        def fix_math_tag(tag, math):
            tag.name = 'pre'
            tag.string = ''
            tag['class'] = tag.get('class', []) + ['math']
            filename, depth = generate_math_image(math)
            tag.append(make_math_img_tag(content_parsed,
                                         math,
                                         path.relpath(filename, pelican_output_path),
                                         depth))

        find_all_math(content_parsed, fix_math_tag)
        article._content = content_parsed.decode()

        clear_memoization_cache(article.get_content)
    return process_article

def find_all_math(document, action):
    for blockquote in document.find_all('blockquote'):
        for math in blockquote.find('pre', class_='math'):
            action(blockquote, math.string.strip())


def clear_memoization_cache(function):
    if isinstance(function, functools.partial):
        memoize_instance = function.func.__self__
        memoize_instance.cache.clear()


def make_math_img_tag(document, tooltip, filename, depth=None):
   if depth is None:
     return document.new_tag('img', src=filename,
                                    alt=tooltip,
                                    title=tooltip)
   else:
     return document.new_tag('img', src=filename,
                                    style="vertical-align: {}px".format(-depth),
                                    alt=tooltip,
                                    title=tooltip)


def configure_docutils_math(pelicanobj):
    docutils_settings = pelicanobj.settings.get('DOCUTILS_SETTINGS', {})
    docutils_settings.setdefault('math_output', 'LaTeX')
    pelicanobj.settings['DOCUTILS_SETTINGS'] = docutils_settings
