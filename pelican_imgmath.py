# -*- coding: utf-8 -*-
"""
    pelican_imgmath
    ~~~~~~~~~~~~~~~~~~
    Render math in HTML via dvipng or dvisvgm.

    :copyright: Copyright 2007-2018 by the Sphinx team and Edward Brown, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

from pelican import signals, generators
from pelican.utils import mkdir_p

import shutil
import tempfile

from hashlib import sha1
from six import string_types
from os import path

from .dvi_to_image import make_dvi_to_png_converter, make_dvi_to_svg_converter, read_png_depth
from .latex import make_latex_to_dvi_converter, make_latex_macro_generator
from .article_html_postprocess import configure_docutils_math, make_article_processor
from .configuration import canonical_settings_as_dictionary, compulsory

def resolve_image_converter(tempdir, settings):
    svg_converter = make_dvi_to_svg_converter(
                        tempdir,
                        compulsory('dvisvgm.command', string_types, settings),
                        compulsory('dvisvgm.args', list, settings))

    png_converter = make_dvi_to_png_converter(
                        tempdir,
                        compulsory('dvipng.command', string_types, settings),
                        compulsory('dvipng.args', list, settings),
                        compulsory('latex.use_preview', bool, settings))

    image_format = compulsory('image_format', string_types, settings).lower()

    image_converter = choose_image_converter(
                            image_format,
                            svg_converter=svg_converter,
                            png_converter=png_converter)

    depth_reader = (read_png_depth if image_format == 'png' else lambda x: None)

    return image_format, image_converter, depth_reader

def resolve_latex_generator(settings):
    return make_latex_macro_generator(
                        compulsory('font_size', int, settings),
                        compulsory('latex.preamble', string_types, settings),
                        compulsory('latex.use_preview', bool, settings))

def pelican_init(pelicanobj):
    configure_docutils_math(pelicanobj)
    postprocess_all_articles.settings = canonical_settings_as_dictionary(pelicanobj)
    postprocess_all_articles.output_path = pelicanobj.output_path

def postprocess_all_articles(content_generators):
    settings = postprocess_all_articles.settings
    output_path = postprocess_all_articles.output_path

    tempdir = tempfile.mkdtemp()

    latex_macro_generator = resolve_latex_generator(settings)
    latex_to_dvi_converter = make_latex_to_dvi_converter(
                        tempdir,
                        compulsory('latex.command', string_types, settings),
                        compulsory('latex.args', list, settings))
    image_format, image_converter, depth_from_image = resolve_image_converter(tempdir, settings)

    filename_generator = make_sha_filename_generator(image_format)
    image_generator = make_image_generator(filename_generator,
                                           latex_macro_generator,
                                           latex_to_dvi_converter,
                                           image_converter,
                                           depth_from_image,
                                           path.join(output_path,
                                                     compulsory('output_directory', string_types, settings)))

    process_article = make_article_processor(image_generator, output_path)

    for generator in content_generators:
        if isinstance(generator, generators.ArticlesGenerator):
            for article in generator.articles:
                process_article(article)
            for translation in generator.translations:
                process_article(translation)
            for draft in generator.drafts:
                process_article(article)
        elif isinstance(generator, generators.PagesGenerator):
            for page in generator.pages:
                process_article(page)

    shutil.rmtree(tempdir)

def make_sha_filename_generator(image_format):
    def filename_from_sha_of(latex):
        return sha1(latex.encode('utf-8')).hexdigest() + "." + image_format

    return filename_from_sha_of

def make_image_generator(generate_filename,
                         generate_latex_macro,
                         latex_to_dvi,
                         dvi_to_image,
                         depth_from_image,
                         image_directory):
    def generate_image_for_math(math):
        output_filename = generate_filename(math)
        output_filepath = path.join(image_directory, output_filename)

        if path.isfile(output_filepath):
            depth = depth_from_image(output_filepath)
            return output_filepath, depth
        else:
            latex = generate_latex_macro(math)
            image_filepath, depth = dvi_to_image(latex_to_dvi(latex))

            mkdir_p(path.dirname(output_filepath))
            shutil.move(image_filepath, output_filepath)

        return output_filepath, depth

    return generate_image_for_math

def choose_image_converter(image_format, svg_converter, png_converter):
    if image_format == 'svg':
        return svg_converter
    elif image_format == 'png':
        return png_converter
    else:
        raise RuntimeError('The format specified by image_format is not supported, pick from svg or png.')

def register():
    signals.initialized.connect(pelican_init)
    signals.all_generators_finalized.connect(postprocess_all_articles)
